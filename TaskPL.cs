﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using TaskBLayer;

namespace ToDo
{
    class TaskPL
    {
        static void Main(string[] args)
        {

            // its merge conflict
            
           
            ManagerBL managerBL = new ManagerBL();
            int i = 1;

            Console.WriteLine("Welcome Task Management System");
            Console.WriteLine("====================================");
            char ch = 'Y';
            do
            {
                ToDoTask task = new ToDoTask();
                

                Console.WriteLine("\nPlease Select your choice");
                Console.WriteLine("-------------------------------");
                Console.WriteLine("1. Add Task and its Details");
                Console.WriteLine("2. Edit Task and its Details");
                Console.WriteLine("3. Mark the task which is Completed");
                Console.WriteLine("4. Get sorted Task list based on Completed/Not Completed");
                Console.WriteLine("5. Get sorted Task list based date");
                Console.WriteLine("6. Exit");
                

                try
                {
                    int type = int.Parse(Console.ReadLine());
                    switch (type)
                    {
                        case 1:
                            
                            task.Id = i++;
                            Console.WriteLine("Enter Task Name");
                            task.Name = Console.ReadLine();

                            Console.WriteLine("Enter the Date(YYYY/MM/DD) ");
                            task.TaskDate = Convert.ToDateTime.Value(Console.ReadLine());

                            managerBL.AddTask(task);

                            break;

                        case 2:

                            Console.WriteLine("Enter Id of Task which needs to be changed ");
                            int id = Convert.ToInt32(Console.ReadLine());

                            Console.WriteLine("Enter Task Name");
                             string name = Console.ReadLine();

                            Console.WriteLine("Enter the Date(YYYY/MM/DD) ");
                            DateTime date = Convert.ToDateTime(Console.ReadLine());

                            managerBL.EditTask(id,name,date);

                            Console.WriteLine("--------------------------------------------------------");
                            foreach (var item in managerBL.EditTask(id, name, date))
                            {
                                Console.WriteLine($"Id :{item.Id}\nName :{item.Name}\nDate :{item.TaskDate.Date.ToString("MM/dd/yyyy")}\nIscompleted :{item.Iscompleted}");
                            }
                            break;

                        case 3:

                            Console.WriteLine("Enter Id of Task which is completed ");
                            int ids = Convert.ToInt32(Console.ReadLine());

                            managerBL.TaskCompleted(ids);

                            break;

                        case 4:
                           
                            Console.WriteLine("--------------------------------------------------------");
                            foreach (var item in managerBL.SortTaskByIsCompleted())
                            {
                                Console.WriteLine($"Id :{item.Id}\nName :{item.Name}\nDate :{item.TaskDate.Date.ToString("MM/dd/yyyy")}\nIscompleted :{item.Iscompleted}"); ;
                            }

                            break;

                        case 5:
                           
                            Console.WriteLine("--------------------------------------------------------");
                            foreach (var item in managerBL.SortTaskByDate())
                            {
                                Console.WriteLine($"Id :{item.Id}\nName :{item.Name}\nDate :{item.TaskDate.Date.ToString("MM/dd/yyyy")}\nIscompleted :{item.Iscompleted}");
                            }

                            break;

                        case 6:
                            ch = 'N';

                            break;

                        default:

                            Console.Write("\nPlease Enter the Choice Correctly!! Please Try Again !! Thank you\n");
                            break;
                    }
                    //Console.WriteLine("Do you want to continue? Y/N");
                    //ch = char.Parse(Console.ReadLine().ToUpper());

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


            } while (ch == 'Y');
        }
    }
}
